﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class PlayerStart : MonoBehaviour
{
    public float moveSpeed = 5f;

    public GameObject Textbox;
    public GameObject TextboxText;
    public GameObject Juul;
    public GameObject SlimJim;
    public GameObject AlexJonesMemoir;
    public AudioSource click;
    public AudioSource popup;
    public AudioSource stepsOut;
    public Rigidbody2D rb;
    public Image blackout;
    public Animator animator;
    public int Next;
    public int dialogueoption;

    public static bool JuulPickedUp;
    public static bool SlimPickedUp;
    public static bool AlexPickedUp;
    public static int SceneNumber;
    public bool disableWalk;
    private bool fadeOut;
    Vector2 movement;
    

    void Start()
    {
        Textbox.SetActive(true);
        TextboxText.GetComponent<Text>().text = "(Press Z Button to Continue...)";
        Next = 0;
        dialogueoption = 0;
        disableWalk = true;
        JuulPickedUp = false;
        SlimPickedUp = false;
        AlexPickedUp = false;
        SceneNumber = 1;

        blackout.canvasRenderer.SetAlpha(0.0f);
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            click.Play();
        }

        if (Next == 0 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"I'm finally up...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 1 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"It's time to go to my first in-person class...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            } 
        if (Next == 2 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"I just hope I don't have any trouble getting there...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 3 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"There are some real characters in this town...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 4 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"I'll leave out the door once I'm ready...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 5 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"But first, I should grab some supplies...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 6 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "(Use the ARROW KEYS to move around.)";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 7 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Invoke("ContinueReset", 0.1f);
                Invoke("TurnOffTextBox", 0.1f);
                Invoke("TurnOnWalk", 0.1f);
                click.Play(0);
            }

        if (Next == 0 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Good thing I quit vaping a while ago...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 1 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"But I'll take it...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            } 
        if (Next == 2 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Just in case.\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 3 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "Juul added to your inventory.";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 4 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                Invoke("ContinueReset", 0.1f);
                Invoke("TurnOffTextBox", 0.1f);
                Invoke("TurnOnWalk", 0.1f);
                Juul.SetActive(false);
                JuulPickedUp = true;
                click.Play(0);
            }
        
        if (Next == 0 && dialogueoption == 2 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Somebody got it for me as a joke...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 1 && dialogueoption == 2 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"That guy sure is crazy...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            } 
        if (Next == 2 && dialogueoption == 2 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"But he's fascinating to watch.\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 3 && dialogueoption == 2 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "Alex Jones Memoir has been added to your inventory.";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 4 && dialogueoption == 2 && Input.GetKeyDown(KeyCode.Z))
            {
                Invoke("ContinueReset", 0.1f);
                Invoke("TurnOffTextBox", 0.1f);
                Invoke("TurnOnWalk", 0.1f);
                AlexJonesMemoir.SetActive(false);
                AlexPickedUp = true;
                click.Play(0);
            }

        if (Next == 0 && dialogueoption == 3 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Snap into a Slim Jim brother...\"";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 1 && dialogueoption == 3 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "Slim Jim has been added to your inventory.";
                Invoke("Continue", 0.1f);
                click.Play(0);
            }
        if (Next == 2 && dialogueoption == 3 && Input.GetKeyDown(KeyCode.Z))
            {
                Invoke("ContinueReset", 0.1f);
                Invoke("TurnOffTextBox", 0.1f);
                Invoke("TurnOnWalk", 0.1f);
                SlimJim.SetActive(false);
                SlimPickedUp = true;
                click.Play(0);
            }
        
        // Input
        
        if(disableWalk == false)
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");

            animator.SetFloat("Horizontal", movement.x);
            animator.SetFloat("Vertical", movement.y);
            animator.SetFloat("Speed", movement.sqrMagnitude);
        }

        if(fadeOut == true)
        {
            FadeOut();
        }
    }

    void FixedUpdate()
    {
        // Movement
        if(disableWalk == false)
        {
            rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Door"))
        {
            Invoke("IntoBattle", 2.0f);
            Debug.Log("You triggered the collider");
            disableWalk = true;
            fadeOut = true;
            stepsOut.Play(0);
        }

        if(other.gameObject.CompareTag("JuulArea"))
        {
            Textbox.SetActive(true);
            TextboxText.GetComponent<Text>().text = "\"Ah look, my old trusty Juul...\"";
            popup.Play(0);
            disableWalk = true;
            dialogueoption = 1;
        }

        if(other.gameObject.CompareTag("AlexJonesMemoirArea"))
        {
            Textbox.SetActive(true);
            TextboxText.GetComponent<Text>().text = "\"There's my Alex Jones memoir...\"";
            disableWalk = true;
            dialogueoption = 2;
            popup.Play(0);
        }

        if(other.gameObject.CompareTag("SlimJimArea"))
        {
            Textbox.SetActive(true);
            TextboxText.GetComponent<Text>().text = "\"That's a classic right there...\"";
            disableWalk = true;
            dialogueoption = 3;
            popup.Play(0);
        }
    }

    void Continue()
    {
        Next += 1;
    }

    void TurnOffTextBox()
    {
        Textbox.SetActive(false);
    }

    void ContinueReset()
    {
        Next = 0;
        dialogueoption = 10;
    }

    void TurnOnWalk()
    {
        disableWalk = false;
    }

    void FadeOut()
    {
        blackout.CrossFadeAlpha(1, 2, false);
    }

    void IntoBattle()
    {
        SceneManager.LoadScene("JuulKidBattle");
    }
}
