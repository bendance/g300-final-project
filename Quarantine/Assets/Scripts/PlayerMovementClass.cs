﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovementClass : MonoBehaviour
{
    public float moveSpeed = 5f;

    public GameObject Textbox;
    public GameObject TextboxText;
    public Rigidbody2D rb;
    public Animator animator;
    public int Next;
    public int dialogueoption;

    public AudioSource click;
    public AudioSource popup;
    public AudioSource music;

    public bool thresholdReached = false;
    bool disableWalk;
    Vector2 movement;
    


    void Start()
    {
        Textbox.SetActive(false);
        Next = 0;
        dialogueoption = 0;
        disableWalk = false;
        TextboxText.GetComponent<Text>().text = "\"I'm finally here...\"";
    }
    // Update is called once per frame
    void Update()
    {
        if(thresholdReached == true)
        {
            disableWalk = true;
            Textbox.SetActive(true);
        }

        if(Input.GetKeyDown(KeyCode.Z))
        {
            click.Play();
        }
            
        if (Next == 0 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Time for school to be back to normal...\"";
                Invoke("Continue", 0.1f);
                click.Play();
            } 
        if (Next == 1 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Wait, where is everyone...\"";
                Invoke("Continue", 0.1f);
                click.Play();
            }
        if (Next == 2 && dialogueoption == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                thresholdReached = false;
                Invoke("ContinueReset", 0.1f);
                Invoke("TurnOffTextBox", 0.1f);
                Invoke("TurnOnWalk", 0.1f);
                click.Play();
            }

        if (Next == 0 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"I wasn't even supposed to come to class today...\"";
                Invoke("Continue", 0.1f);
                click.Play();
            }
        if (Next == 1 && dialogueoption == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                TextboxText.GetComponent<Text>().text = "\"Dang it.\"";
                Invoke("TheEnd", 2.0f);
                click.Play();
            }
        
        // Input
        if(disableWalk == false)
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");

            animator.SetFloat("Horizontal", movement.x);
            animator.SetFloat("Vertical", movement.y);
            animator.SetFloat("Speed", movement.sqrMagnitude);
        }
        
    }

    void FixedUpdate()
    {
        // Movement
        if(disableWalk == false)
        {
            rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Threshold"))
        {
            popup.Play();
            thresholdReached = true;
        }

        if(other.gameObject.CompareTag("Nobody"))
        {
            popup.Play();
            dialogueoption = 1;
            music.Stop();
            thresholdReached = true;
            TextboxText.GetComponent<Text>().text = "\"Oh wait, I just remembered...\"";
        }
    }

    void Continue()
    {
        Next += 1;
    }

    void TurnOffTextBox()
    {
        Textbox.SetActive(false);
    }

    void ContinueReset()
    {
        Next = 0;
        dialogueoption = 10;
    }

    void TurnOnWalk()
    {
        disableWalk = false;
    }

    void TheEnd()
    {
        SceneManager.LoadScene("TheEnd");
    }
}
