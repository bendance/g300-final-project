﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHUD : MonoBehaviour
{
    public Text nameText;
    public Text levelText;
    public Text hpText;
    public Text maxhpText;


    public void SetHUD(Unit unit)
    {
        nameText.text = unit.unitName;
        levelText.text = "Lvl " + unit.unitLevel;
        hpText.text = unit.currentHP.ToString();
        maxhpText.text = "/" + unit.maxHP.ToString();
    }

    public void SetHP(int hp)
    {
        hpText.text = hp.ToString();
    }
}
