﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChoiceScript : MonoBehaviour
{
    public GameObject Textbox;
    public GameObject Option_1;
    public GameObject Option_2;
    public GameObject Option_3;
    public GameObject Items;
    public GameObject Item_1;
    public GameObject Item_2;
    public GameObject Item_3;
    public GameObject BackButton;
    public GameObject YouWinScreen;
    public GameObject YouLoseScreen;
    public GameObject UsageText;
    public GameObject JuulKid;

    public AudioSource NatureSounds;
    public AudioSource NextDialogue;
    public AudioSource BossMusic;
    public AudioSource OptionSelect;
    public AudioSource YouWinSound;
    public AudioSource YouLoseSound;

    public int ChoiceMade;
    public int Selection;
    public int Next;
    
    bool YouWon = false;
    bool YouLost = false;
    bool disableButtons = false;
    float buttondisabletime = 2.0f;

    void Start()
    {
        Textbox.GetComponent<Text>().text = "(Use the MOUSE to select an option...)";
        Option_1.GetComponentInChildren<Text>().text = "\"I'm in a rush right now, sorry\"";
        Option_2.GetComponentInChildren<Text>().text = "\"Aren't you a little young?\"";
        Option_3.GetComponentInChildren<Text>().text = "Ignore him";
        Items.GetComponentInChildren<Text>().text = "Items";
        ChoiceMade = 0;
        Selection = 0;
        disableButtons = true;
        Next = 0;
        JuulKid.SetActive(false);
        CountdownTimer.startTimer = false;
        PlayerStart.SceneNumber = 2;
        
        YouWinScreen.SetActive(false);
        UsageText.SetActive(false);
        YouLoseScreen.SetActive(false);   
    }

    public void Option1()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 1;
            Textbox.GetComponent<Text>().text = "\"Please man, I'm fiending so bad.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 1)
        {
            Next = 0;
            Selection = 4;
            Textbox.GetComponent<Text>().text = "He starts chasing you...";
            disableButtons = true;
        }

        if(ChoiceMade == 4)
        {
            Next = 0;
            Selection = 7;
            Textbox.GetComponent<Text>().text = "He starts chasing you...";
            disableButtons = true;
        }

        if(ChoiceMade == 7)
        {
            Next = 0;
            Selection = 1;
            Textbox.GetComponent<Text>().text = "\"Please man, I'm fiending so bad.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 8)
        {
            Next = 0;
            Selection = 11;
            Textbox.GetComponent<Text>().text = "\"Uhh...dude.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 9)
        {
            Next = 0;
            Selection = 12;
            Textbox.GetComponent<Text>().text = "\"Okay...\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 13)
        {
            Next = 0;
            Selection = 15;
            Textbox.GetComponent<Text>().text = "You run into the alley...";
            disableButtons = true;
        }

        if(ChoiceMade == 14)
        {
            Next = 0;
            Selection = 50;
            Textbox.GetComponent<Text>().text = "The kid stares at you blankly.";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 15)
        {
            Next = 0;
            Selection = 18;
            Textbox.GetComponent<Text>().text = "\"Holy heck, please no.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 18)
        {
            Next = 0;
            Selection = 21;
            Textbox.GetComponent<Text>().text = "You run into a parking garage...";
            disableButtons = true;
        }

        if(ChoiceMade == 20)
        {
            Next = 0;
            Selection = 75;
            Textbox.GetComponent<Text>().text = "\"Wow, thanks man...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 25)
        {
            Next = 0;
            Selection = 27;
            Textbox.GetComponent<Text>().text = "The cop looks disappointed...";
            disableButtons = true;
        }

        if(ChoiceMade == 29)
        {
            Next = 0;
            Selection = 31;
            Textbox.GetComponent<Text>().text = "The cop puts his hand on his gun.";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 32)
        {
            Next = 0;
            Selection = 22;
            Textbox.GetComponent<Text>().text = "The cop asks, \"What seems to be the problem here?\" ";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 36)
        {
            Next = 0;
            Selection = 39;
            Textbox.GetComponent<Text>().text = "You tell the cop that you're buying the Juul for a kid...";
            disableButtons = true;
        }

        if(ChoiceMade == 53)
        {
            Next = 0;
            Selection = 56;
            Textbox.GetComponent<Text>().text = "The two of you just stare at each other.";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 59)
        {
            Next = 0;
            Selection = 62;
            Textbox.GetComponent<Text>().text = "\"Dude.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 65)
        {
            Next = 0;
            Selection = 68;
            Textbox.GetComponent<Text>().text = "The kid sits on the side of the curb...";
            disableButtons = true;
        }

        if(ChoiceMade == 71)
        {
            Next = 0;
            Selection = 76;
            Textbox.GetComponent<Text>().text = "The kid walks away...";
            disableButtons = true;
        }

        if(ChoiceMade == 72)
        {
            Next = 0;
            Selection = 99;
            Textbox.GetComponent<Text>().text = "You stop and try to reason with kid...";
            disableButtons = true;
        }

        if(ChoiceMade == 73)
        {
            Next = 0;
            Selection = 98;
            Textbox.GetComponent<Text>().text = "You turn around and realize that he's nowhere to be seen...";
            disableButtons = true;
        }

        if(ChoiceMade == 74)
        {
            Next = 0;
            Selection = 75;
            Textbox.GetComponent<Text>().text = "\"Fine, I won't...\"";
            disableButtons = true;
        }
    }

    public void Option2()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 1;
            Textbox.GetComponent<Text>().text = "\"Please man, I'm fiending so bad.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 1)
        {
            Selection = 5;
            Textbox.GetComponent<Text>().text = "\"Uh...okay...and?\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 5)
        {
            Next = 0;
            Selection = 1;
            Textbox.GetComponent<Text>().text = "\"Please man, I'm fiending so bad.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 7)
        {
            Next = 0;
            Selection = 10;
            Textbox.GetComponent<Text>().text = "The kid keeps chasing after you...";
            disableButtons = true;
        }

        if(ChoiceMade == 8)
        {
            Next = 0;
            Selection = 66;
            Textbox.GetComponent<Text>().text = "\"Please don't, my mom will kill me.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 9)
        {
            Next = 0;
            Selection = 33;
            Textbox.GetComponent<Text>().text = "You begin to buy the Juul...";
            disableButtons = true;
        }

        if(ChoiceMade == 13)
        {
            Next = 0;
            Selection = 88;
            Textbox.GetComponent<Text>().text = "The kid keeps gaining on you, his knife drawn...";
            disableButtons = true;
        }

        if(ChoiceMade == 15)
        {
            Next = 0;
            Selection = 76;
            Textbox.GetComponent<Text>().text = "The kid walks away...";
            disableButtons = true;
        }

        if(ChoiceMade == 18)
        {
            Next = 0;
            Selection = 101;
            Textbox.GetComponent<Text>().text = "You accept your fate as the blade pierces your flesh.";
            disableButtons = true;
            Invoke("YouLose", buttondisabletime);
        }

        if(ChoiceMade == 20)
        {
            Next = 0;
            Selection = 22;
            Textbox.GetComponent<Text>().text = "The kid sits on the curb, questioning his life decisions...";
            disableButtons = true;
        }

        if(ChoiceMade == 25)
        {
            Next = 0;
            Selection = 28;
            Textbox.GetComponent<Text>().text = "The cop looks at you...";
            disableButtons = true;
        }

        if(ChoiceMade == 29)
        {
            Next = 0;
            Selection = 31;
            Textbox.GetComponent<Text>().text = "The cop puts his hand on his gun...";
            disableButtons = true;
        }

        if(ChoiceMade == 30)
        {
            Next = 0;
            Selection = 33;
            Textbox.GetComponent<Text>().text = "You begin to buy the Juul...";
            disableButtons = true;
        }

        if(ChoiceMade == 32)
        {
            Next = 0;
            Selection = 34;
            Textbox.GetComponent<Text>().text = "The cop handcuffs you and lays you down on the street...";
            disableButtons = true;
        }

        if(ChoiceMade == 16)
        {
            Next = 0;
            Selection = 19;
            Textbox.GetComponent<Text>().text = "\"Holy heck, please no.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 36)
        {
            Next = 0;
            Selection = 40;
            Textbox.GetComponent<Text>().text = "\"No sir,\" you say with a smile...";
            disableButtons = true;
        }

        if(ChoiceMade == 62)
        {
            Next = 0;
            Selection = 66;
            Textbox.GetComponent<Text>().text = "\"Please don't, my mom will kill me.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 65)
        {
            Next = 0;
            Selection = 66;
            Textbox.GetComponent<Text>().text = "\"Please don't, my mom will kill me.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 68)
        {
            Next = 0;
            Selection = 66;
            Textbox.GetComponent<Text>().text = "\"Please don't, my mom will kill me.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 71)
        {
            Next = 0;
            Selection = 66;
            Textbox.GetComponent<Text>().text = "\"Please don't, my mom will kill me.\"";
            disableButtons = true;
            Invoke("DisableButtons", buttondisabletime);
        }

        if(ChoiceMade == 72)
        {
            Next = 0;
            Selection = 74;
            Textbox.GetComponent<Text>().text = "You keep running from the kid...";
            disableButtons = true;
        }

        if(ChoiceMade == 74)
        {
            Next = 0;
            Selection = 80;
            Textbox.GetComponent<Text>().text = "The kid runs away...";
            disableButtons = true;
        }
    }

    public void Option3()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 4;
            Textbox.GetComponent<Text>().text = "He starts chasing you...";
            disableButtons = true;
        }

        if(ChoiceMade == 1)
        {
            Next = 0;
            Selection = 6;
            Textbox.GetComponent<Text>().text = "\"Bro please, the gas station is right over there...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 36)
        {
            Next = 0;
            Selection = 41;
            Textbox.GetComponent<Text>().text = "You decide to make a run for it...";
            disableButtons = true;
        }
    }

    public void Option4()
    {
        OptionSelect.Play();

        disableButtons = true;
        Invoke("TurnOnItems", 0.1f);
    }

    public void Option5()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 200;
            Textbox.GetComponent<Text>().text = "\"WOW...\"";
            disableButtons = true;
            PlayerStart.JuulPickedUp = false;
            Invoke("TurnOffItems", 0.1f);
        }
        else
        {
            Invoke("CannotUse", 0.1f);
        }
    
    }

    public void Option6()
    {
        OptionSelect.Play();

        if(ChoiceMade == 36)
        {
            Next = 0;
            Selection = 205;
            Textbox.GetComponent<Text>().text = "\"Oh, I see...\"";
            disableButtons = true;
            Invoke("TurnOffItems", 0.1f);
        }
        else
        {
            Invoke("CannotUse", 0.1f);
        }
    }

    public void Option7()
    {
        OptionSelect.Play();

        Invoke("CannotUse", 0.1f);
    }

    public void Option8()
    {
        OptionSelect.Play();

        Invoke("TurnOffItems", 0.1f);
        Invoke("DisableButtons", 0.1f);
        Textbox.SetActive(true);
        UsageText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            NextDialogue.Play();
        }
        //This is for item booleans

        if(PlayerStart.JuulPickedUp == true)
        {
            Item_1.GetComponentInChildren<Text>().text = "Juul";
        }

        if(PlayerStart.JuulPickedUp == false)
        {
            Item_1.SetActive(false);
        }

        if(PlayerStart.AlexPickedUp == true)
        {
            Item_3.GetComponentInChildren<Text>().text = "Alex Jones Memoir";
        }

        if(PlayerStart.AlexPickedUp == false)
        {
            Item_3.SetActive(false);
        }

        if(PlayerStart.SlimPickedUp == true)
        {
            Item_2.GetComponentInChildren<Text>().text = "Slim Jim";
        }

        if(PlayerStart.SlimPickedUp == false)
        {
            Item_2.SetActive(false);
        }
        //This is for continue

        if (Next == 0 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "(Don't let the TIMER reach 00...)";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "(Or you'll end up catching COVID-19...)";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Well I'm on my way to school...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "And life is good...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 4 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "It doesn't seem like anything can go wrong...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 5 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Hey man...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 6 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                CountdownTimer.startTimer = true;
                JuulKid.SetActive(true);
                Textbox.GetComponent<Text>().text = "\"You got a Juul?\"";
                Invoke("DisableButtons", buttondisabletime);
            }

        if (Selection == 4 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Please, I need this.\" ";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 7 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Please, I need this.\" ";
                NextDialogue.Play();
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 12 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Well, please don't tell anybody.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 15 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "It's a dead end. The kid brandishes a knife at you.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "And hide behind one of the cars...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You see the kids feet drag on the concrete...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Suddenly, the kid's mother calls for him...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }

        if (Next == 3 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Gerald! Where are you, honey?..\" ";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 4 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Coming mom!..\" ";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 5 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The kid goes back to his mother. You're safe...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 6 && Selection == 21 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "For now.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 75 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The kid walks away...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 75 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "But not before coughing on you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }

        if (Next == 2 && Selection == 75 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You're not a preacher, buddy.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 76 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "But not before coughing on you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }

        if (Next == 1 && Selection == 76 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You're not a preacher, buddy.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 27 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He handcuffs the kid and sits him in the back of his car...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 27 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Looks like you're going to be here for a while.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 31 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Sir, I'm going to have to ask you to stop.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 39 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The cop promptly arrests you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 39 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Can't believe you almost bought a juul for a kid...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 39 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Dang.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 56 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He looks at you with contempt.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 68 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He starts coughing.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 99 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You feel an icy sensation in your chest...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 99 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You slowly lay yourself down and try to call an ambulance...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 99 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The kid runs away.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 98 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You must have lost him.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 10 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "All the late night ramen noodles haven't made you any faster...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 10 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You see an alley to the side.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 33 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "A police officer barges in and stares you down...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 33 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"There's some kid outside who keeps asking people to buy him a Juul...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 33 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Do you know anything about that, sir?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 88 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You feel yourself slow down.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 22 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "All the sudden a cop arrives...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 22 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Alright now, what appears to be the problem here.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 28 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Are you harrassing me?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 34 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Looks like you aren't going to class today.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 40 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Here's that Juul you bought, sir...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 40 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"I noticed that you and that kid talking for a while...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 40 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Do you know him or something?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 40 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The cop looks at you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 4 && Selection == 40 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Looks like you're not going to class anytime soon.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 74 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "But you feel him gaining on you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 74 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You feel totally winded.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 6 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Here's sixty dollars, just go in and buy me one.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 41 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The cop doesn't even chase you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 41 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You see the kid sitting on a street corner in handcuffs...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 41 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Another officer pins you to the ground and handcuffs you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 41 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Looks like you won't make it to school today.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 80 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "I guess he didn't want any trouble.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Selection == 200 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Thank you so much, mister.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 205 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The cop proceeds to question other customers...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 205 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Looks like you got away with it.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }
            
        // This is for all the dialogue stuff
        if(disableButtons == true)
        {
            Option_1.SetActive(false);
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            Items.SetActive(false);
        }

        if(Selection == 1)
        {
            Option_1.GetComponentInChildren<Text>().text = "Keep walking away";
            Option_2.GetComponentInChildren<Text>().text = "\"Don't you know vaping's bad for you?\"";
            Option_3.GetComponentInChildren<Text>().text = "\"I'm really in a rush today, buddy\"";
            ChoiceMade = 1;
        }

        if(Selection == 4)
        {
            Option_1.GetComponentInChildren<Text>().text = "You decide to stop and listen to him";
            Option_2.GetComponentInChildren<Text>().text = "You keep running";
            Option_3.SetActive(false);
            ChoiceMade = 7;
        }

        if(Selection == 5)
        {
            Option_1.GetComponentInChildren<Text>().text = "Keep telling him about why vaping is bad";
            Option_2.GetComponentInChildren<Text>().text = "Tell his mother";
            Option_3.SetActive(false);
            ChoiceMade = 8;
        }

        if(Selection == 6)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"No way\"";
            Option_2.GetComponentInChildren<Text>().text = "\"Okay\"";
            Option_3.SetActive(false);
            ChoiceMade = 9;
        }

        if(Selection == 10)
        {
            Option_1.GetComponentInChildren<Text>().text = "Run into the alley";
            Option_2.GetComponentInChildren<Text>().text = "Keep running straight";
            Option_3.SetActive(false);
            ChoiceMade = 13;
        }

        if(Selection == 11)
        {
            Option_1.GetComponentInChildren<Text>().text = "Tell him about the harmful metals";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 14;
        }

        if(Selection == 12)
        {
            Option_1.GetComponentInChildren<Text>().text = "Call the police";
            Option_2.GetComponentInChildren<Text>().text = "Tell nobody";
            Option_3.SetActive(false);
            ChoiceMade = 15;
        }

        if(Selection == 15)
        {
            Option_1.GetComponentInChildren<Text>().text = "Run into the garage";
            Option_2.GetComponentInChildren<Text>().text = "Accept your fate";
            Option_3.SetActive(false);
            ChoiceMade = 18;
        }

        if(Selection == 18)
        {
            Option_1.GetComponentInChildren<Text>().text = "You don't call 911 after all";
            Option_2.GetComponentInChildren<Text>().text = "You begin calling 911";
            Option_3.SetActive(false);
            ChoiceMade = 20;
        }

        if(Selection == 21)
        {
            Option_1.GetComponentInChildren<Text>().text = "Run into the garage";
            Option_2.GetComponentInChildren<Text>().text = "Accept your fate";
            Option_3.SetActive(false);
            ChoiceMade = 24;
        }

        if(Selection == 22)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"This kid is asking people to buy him a juul\"";
            Option_2.GetComponentInChildren<Text>().text = "\"Start making pig noises\"";
            Option_3.SetActive(false);
            ChoiceMade = 25;
        }

        if(Selection == 28)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"Yep\"";
            Option_2.GetComponentInChildren<Text>().text = "\"Nope, just a tick\"";
            Option_3.SetActive(false);
            ChoiceMade = 29;
        }

        if(Selection == 31)
        {
            Option_1.GetComponentInChildren<Text>().text = "You comply";
            Option_2.GetComponentInChildren<Text>().text = "You keep making pig noises";
            Option_3.SetActive(false);
            ChoiceMade = 32;
        }

        if(Selection == 33)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"Yep\"";
            Option_2.GetComponentInChildren<Text>().text = "\"No sir\"";
            Option_3.GetComponentInChildren<Text>().text = "Make a run for it";
            ChoiceMade = 36;
        }

        if(Selection == 50)
        {
            Option_1.GetComponentInChildren<Text>().text = "Tell him about the risk of heart attack";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 53;
        }

        if(Selection == 56)
        {
            Option_1.GetComponentInChildren<Text>().text = "Tell him it's not cuul to juul";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 59;
        }

        if(Selection == 62)
        {
            Option_1.GetComponentInChildren<Text>().text = "Keep telling him about negative side effects";
            Option_2.GetComponentInChildren<Text>().text = "Ask to speak with his mother";
            Option_3.SetActive(false);
            ChoiceMade = 65;
        }

        if(Selection == 66)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"You have to promise not to juul again\"";
            Option_2.GetComponentInChildren<Text>().text = "Actually tell his mother";
            Option_3.SetActive(false);
            ChoiceMade = 74;
        }

        if(Selection == 68)
        {
            Option_1.GetComponentInChildren<Text>().text = "Tell him he's coughing because of the juul";
            Option_2.GetComponentInChildren<Text>().text = "Ask to speak with his mother";
            Option_3.SetActive(false);
            ChoiceMade = 71;
        }

        if(Selection == 74)
        {
            Option_1.GetComponentInChildren<Text>().text = "You give up and stop running";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 73;
        }

        if(Selection == 88)
        {
            Option_1.GetComponentInChildren<Text>().text = "Try to reason with the kid";
            Option_2.GetComponentInChildren<Text>().text = "Keep running";
            Option_3.SetActive(false);
            ChoiceMade = 72;
        }
    }

    void Continue()
    {
        Next += 1;
    }

    void YouWin()
    {
        if(!YouWon)
        {
            YouWinScreen.SetActive(true);
            Invoke("NextScene", 5.0f);
            BossMusic.Stop();
            YouWinSound.Play();
            YouWon = true;
        }
    }

    void YouLose()
    {
        if(!YouLost)
        {
            YouLoseScreen.SetActive(true);
            Invoke("ResetScene", 5.0f);
            BossMusic.Stop();
            YouLoseSound.Play();
            YouLost = true;
        }
    }

    void NextScene()
    {
        SceneManager.LoadScene("AntiVaxPete");
    }

    void ResetScene()
    {
        SceneManager.LoadScene("JuulKidBattle");
    }

    void DisableButtons()
    {
        Option_1.SetActive(true);
        Option_2.SetActive(true);
        Option_3.SetActive(true);
        Items.SetActive(true);
        disableButtons = false;
    }

    void TurnOnItems()
    {
        if(PlayerStart.JuulPickedUp == false)
        {
            Item_1.SetActive(false);
        }
        else
        {
            Item_1.SetActive(true);
        }

        if(PlayerStart.SlimPickedUp == false)
        {
            Item_2.SetActive(false);
        }
        else
        {
            Item_2.SetActive(true);
        }
        
        if(PlayerStart.AlexPickedUp == false)
        {
            Item_3.SetActive(false);
        }
        else
        {
            Item_3.SetActive(true);
        }
        BackButton.SetActive(true);
    }

    void TurnOffItems()
    {
        Item_1.SetActive(false);
        Item_2.SetActive(false);
        Item_3.SetActive(false);
        BackButton.SetActive(false);
    }

    void CannotUse()
    {
        Textbox.SetActive(false);
        UsageText.SetActive(true);
        Invoke("ReturnTextbox", 1.0f);
    }

    void ReturnTextbox()
    {
        Textbox.SetActive(true);
        UsageText.SetActive(false);
    }
}
