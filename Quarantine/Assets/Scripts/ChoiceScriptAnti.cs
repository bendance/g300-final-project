﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChoiceScriptAnti : MonoBehaviour
{
    public GameObject Textbox;
    public GameObject Option_1;
    public GameObject Option_2;
    public GameObject Option_3;
    public GameObject Items;
    public GameObject Item_1;
    public GameObject Item_2;
    public GameObject Item_3;
    public GameObject BackButton;
    public GameObject YouWinScreen;
    public GameObject YouLoseScreen;
    public GameObject UsageText;
    public GameObject AntiVaxPete;

    public AudioSource NatureSounds;
    public AudioSource NextDialogue;
    public AudioSource BossMusic;
    public AudioSource OptionSelect;
    public AudioSource YouWinSound;
    public AudioSource YouLoseSound;

    public int ChoiceMade;
    public int Selection;
    public int Next;
    
    bool YouWon = false;
    bool YouLost = false;
    bool disableButtons;
    float buttondisabletime = 2.0f;

    void Start()
    {
        Textbox.GetComponent<Text>().text = "You feel winded after that first battle...";
        Option_1.GetComponentInChildren<Text>().text = "\"They're cool\"";
        Option_2.GetComponentInChildren<Text>().text = "\"They're lame\"";
        Option_3.GetComponentInChildren<Text>().text = "\"I don't care\"";
        ChoiceMade = 0;
        Selection = 0;
        disableButtons = true;
        Next = 0;
        CountdownTimer.startTimer = false;
        AntiVaxPete.SetActive(false);
        PlayerStart.SceneNumber = 3;

        YouWinScreen.SetActive(false);
        YouLoseScreen.SetActive(false);
    }

    public void Option1()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 1;
            Textbox.GetComponent<Text>().text = "\"They're cool...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 2)
        {
            Next = 0;
            Selection = 3;
            Textbox.GetComponent<Text>().text = "You start walking away from him...";
            disableButtons = true;
        }

        if(ChoiceMade == 5)
        {
            Next = 0;
            Selection = 6;
            Textbox.GetComponent<Text>().text = "You start running away from the deranged fellow...";
            disableButtons = true;
        }

        if(ChoiceMade == 9)
        {
            Next = 0;
            Selection = 10;
            Textbox.GetComponent<Text>().text = "\"Because he's one of the elites...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 51)
        {
            Next = 0;
            Selection = 52;
            Textbox.GetComponent<Text>().text = "\"So you're just going to let big government...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 58)
        {
            Next = 0;
            Selection = 61;
            Textbox.GetComponent<Text>().text = "\"WHAT?..\"";
            disableButtons = true;
        }

        if(ChoiceMade == 64)
        {
            Next = 0;
            Selection = 67;
            Textbox.GetComponent<Text>().text = "The strange man melts into a puddle of sweat...";
            disableButtons = true;
        }
        if(ChoiceMade == 68)
        {
            Next = 0;
            Selection = 69;
            Textbox.GetComponent<Text>().text = "The gang of conspiracy theorists tackle you to the ground...";
            disableButtons = true;
        }

        if(ChoiceMade == 70)
        {
            Next = 0;
            Selection = 71;
            Textbox.GetComponent<Text>().text = "\"What...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 73)
        {
            Next = 0;
            Selection = 74;
            Textbox.GetComponent<Text>().text = "\"Well that's just crazy talk...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 101)
        {
            Next = 0;
            Selection = 105;
            Textbox.GetComponent<Text>().text = "\"You're my kind of people...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 106)
        {
            Next = 0;
            Selection = 107;
            Textbox.GetComponent<Text>().text = "You take the pamphlet from the strange man...";
            disableButtons = true;
        }

        if(ChoiceMade == 125)
        {
            Next = 0;
            Selection = 126;
            Textbox.GetComponent<Text>().text = "\"Lizard people come from the sixth ring of hell...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 127)
        {
            Next = 0;
            Selection = 128;
            Textbox.GetComponent<Text>().text = "\"Yea right...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 129)
        {
            Next = 0;
            Selection = 130;
            Textbox.GetComponent<Text>().text = "You show him your chunk of rotting flesh...";
            disableButtons = true;
        }

        if(ChoiceMade == 155)
        {
            Next = 0;
            Selection = 156;
            Textbox.GetComponent<Text>().text = "You swallow the bug...";
            disableButtons = true;
        }

        if(ChoiceMade == 158)
        {
            Next = 0;
            Selection = 159;
            Textbox.GetComponent<Text>().text = "You swallow another bug...";
            disableButtons = true;
        }
    }

    public void Option2()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 100;
            Textbox.GetComponent<Text>().text = "\"Oh really?..\"";
            disableButtons = true;
        }

        if(ChoiceMade == 2)
        {
            Next = 0;
            Selection = 4;
            Textbox.GetComponent<Text>().text = "You ask the man what his problem is...";
            disableButtons = true;
        }

        if(ChoiceMade == 5)
        {
            Next = 0;
            Selection = 8;
            Textbox.GetComponent<Text>().text = "\"WHO'S CHARLIE SHEEN...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 51)
        {
            Next = 0;
            Selection = 55;
            Textbox.GetComponent<Text>().text = "The man's face turns red with rage...";
            disableButtons = true;
        }

        if(ChoiceMade == 58)
        {
            Next = 0;
            Selection = 59;
            Textbox.GetComponent<Text>().text = "\"Class?..\"";
            disableButtons = true;
        }

        if(ChoiceMade == 64)
        {
            Next = 0;
            Selection = 65;
            Textbox.GetComponent<Text>().text = "\"Sure, sure...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 68)
        {
            Next = 0;
            Selection = 69;
            Textbox.GetComponent<Text>().text = "The gang of conspiracy theorists tackle you to the ground...";
            disableButtons = true;
        }

        if(ChoiceMade == 70)
        {
            Next = 0;
            Selection = 71;
            Textbox.GetComponent<Text>().text = "\"What...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 101)
        {
            Next = 0;
            Selection = 52;
            Textbox.GetComponent<Text>().text = "\"So you're just going to let big government...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 106)
        {
            Next = 0;
            Selection = 55;
            Textbox.GetComponent<Text>().text = "The man's face turns red with rage...";
            disableButtons = true;
        }

        if(ChoiceMade == 127)
        {
            Next = 0;
            Selection = 150;
            Textbox.GetComponent<Text>().text = "The man lets out an inhuman shriek...";
            disableButtons = true;
        }

        if(ChoiceMade == 129)
        {
            Next = 0;
            Selection = 131;
            Textbox.GetComponent<Text>().text = "The man starts melting like an ice cream cone...";
            disableButtons = true;
        }

        if(ChoiceMade == 158)
        {
            Next = 0;
            Selection = 160;
            Textbox.GetComponent<Text>().text = "You apologize for eating the bug...";
            disableButtons = true;
        }
    }

    public void Option3()
    {
        OptionSelect.Play();

        if(ChoiceMade == 0)
        {
            Next = 0;
            Selection = 50;
            Textbox.GetComponent<Text>().text = "\"Oh, you don't care...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 58)
        {
            Next = 0;
            Selection = 55;
            Textbox.GetComponent<Text>().text = "The man's face turns red with rage...";
            disableButtons = true;
        }

        if(ChoiceMade == 68)
        {
            Next = 0;
            Selection = 69;
            Textbox.GetComponent<Text>().text = "The gang of conspiracy theorists tackle you to the ground...";
            disableButtons = true;
        }

        if(ChoiceMade == 70)
        {
            Next = 0;
            Selection = 71;
            Textbox.GetComponent<Text>().text = "\"What...\"";
            disableButtons = true;
        }

        if(ChoiceMade == 101)
        {
            Next = 0;
            Selection = 55;
            Textbox.GetComponent<Text>().text = "The man's face turns red with rage...";
            disableButtons = true;
        }
    }

    public void Option4()
    {
        OptionSelect.Play();

        disableButtons = true;
        Invoke("TurnOnItems", 0.1f);
    }

    public void Option5()
    {
        OptionSelect.Play();

        Invoke("CannotUse", 0.1f);
    }

    public void Option6()
    {
        OptionSelect.Play();

        Invoke("CannotUse", 0.1f);
    }

    public void Option7()
    {
        OptionSelect.Play();

        if(ChoiceMade == 129)
        {
            Next = 0;
            Selection = 205;
            Textbox.GetComponent<Text>().text = "\"Wait a second...\"";
            disableButtons = true;
            Invoke("TurnOffItems", 0.1f);
        }
        else
        {
            Invoke("CannotUse", 0.1f);
        }
    }

    public void Option8()
    {
        OptionSelect.Play();

        Invoke("TurnOffItems", 0.1f);
        Invoke("DisableButtons", 0.1f);
        Textbox.SetActive(true);
        UsageText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            NextDialogue.Play();
        }
        //This is for item booleans

        if(PlayerStart.JuulPickedUp == true)
        {
            Item_1.GetComponentInChildren<Text>().text = "Juul";
        }

        if(PlayerStart.JuulPickedUp == false)
        {
            Item_1.SetActive(false);
        }

        if(PlayerStart.AlexPickedUp == true)
        {
            Item_3.GetComponentInChildren<Text>().text = "Alex Jones Memoir";
        }

        if(PlayerStart.AlexPickedUp == false)
        {
            Item_3.SetActive(false);
        }

        if(PlayerStart.SlimPickedUp == true)
        {
            Item_2.GetComponentInChildren<Text>().text = "Slim Jim";
        }

        if(PlayerStart.SlimPickedUp == false)
        {
            Item_2.SetActive(false);
        }

        //This is for continue
        if (Next == 0 && Selection == 205 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"IS THAT AN ALEX JONES MEMOIR...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 205 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"I had you figuired out all wrong...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 205 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"You're no globalist...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 205 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"You're one of us.\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
                Invoke("YouWin", buttondisabletime);
            }

        if (Next == 0 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "But you manage to continue walking to school...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "A man with a fedora is grumbling to himself in a corner...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He appears to be holding a pamphlet...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Hey buddy...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 4 && Selection == 0 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"What do you think about these new COVID vaccines?\"";
                Invoke("DisableButtons", buttondisabletime);
                CountdownTimer.startTimer = true;
                AntiVaxPete.SetActive(true);
                NextDialogue.Play();
            }           

        if (Next == 0 && Selection == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"THEY'RE COOL!?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 1 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The man starts hyperventilating.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            } 

        if (Next == 0 && Selection == 3 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Hey, HEY!..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 3 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You hear the Facebook conspiracies fade...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            } 
        if (Next == 2 && Selection == 3 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "What a weirdo.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 4 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He tilts his fedora down...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 4 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"My problem is with Hollywood elites microchiping our vaccines...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 4 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Like Charlie Sheen...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 4 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"He's going to put GPS in our vaccines...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }    
        if (Next == 4 && Selection == 4 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"So he can follow us around.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }    

        if (Next == 0 && Selection == 6 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Only to find that a fleet of his goons are chasing you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 6 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "On their mopeds...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 6 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "As your legs slow, you feel them drawing closer...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 6 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "This looks like the end for you.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            } 

        if (Next == 0 && Selection == 8 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Charlie Sheen is a part of the weather control program...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 8 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"HE'S ONE OF THE ELITES...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 8 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"AND HE WANTS TO TAKE YOUR CHILDREN...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 8 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"AND TURN THEM INTO FROG PEOPLE.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }     

        if (Next == 0 && Selection == 10 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"THAT'S WHY...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 10 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"He wants to take your children...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 10 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"AND FEED THEM TO THE LIZARD PEOPLE.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 50 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"What's wrong with you?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 50 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"ARE YOU ONE OF THOSE SHEEPLE?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 50 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Somebody who just goes with the flow?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 52 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"And Bill Gates...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 52 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"And the lizard people on Wall Street...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 52 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Do whatever they want?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }  
        if (Next == 3 && Selection == 52 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"I can't believe you.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 55 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He looks at you like an alien...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 55 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He throws his fists against the wall like an ape...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 55 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "And he smashes your face.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 59 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"What do you need to go to class for?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 59 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"TO LEARN ABOUT THE RULES OF THE GLOBALISTS...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 59 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"PUTTING FLUORIDE IN OUR WATER...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            } 
        if (Next == 3 && Selection == 59 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"CORRUPTING OUR YOUTH?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            } 


        if (Next == 0 && Selection == 61 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "So you're one of the corporate elite...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 61 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Who's been stealing our children...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 61 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "AND GIVING THEM POLIO!?";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 67 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Stay away from me you freak...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 67 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He runs into the sunrise...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 67 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "It's kind of beautiful.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 65 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"That's exactly what a lizard person would say...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 65 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"GET HIM MASONS...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 65 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "A crowd of sweaty men in trenchcoats appear from the sewers...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 65 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You're surrounded.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }   

        if (Next == 0 && Selection == 69 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The start attacking you with their katanas...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 69 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "And Alex Jones memoirs...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 69 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You lie there defeated...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 69 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You filthy lizard person.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 71 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"But video games corrupt the youth...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 71 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Do you really want to live...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 71 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"INSIDE A VIDEO GAME!?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 74 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"And I don't want to hear from a conspiracy theorist...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 74 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Good day to you, sir.\"";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 100 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"So you're a cool guy like me?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 100 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Someone who thinks...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 100 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Outside of the box!?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 105 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"How would you like to join my United Freemasons convention?..\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 105 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"TONIGHT?\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 107 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "And you slowly walk away from him...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 107 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "There are some weirdos out there...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 107 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Good thing you're not going to that convention.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 126 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"They have slimy, scaly skin...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 126 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"They hate the sunlight and feed on insects...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 126 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"LIKE IN THE LION KING.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }  

        if (Next == 0 && Selection == 128 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"I won't let you go that easy...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 128 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"There's no way you could be a lizard person.\"";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 130 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The strange man starts devouring your leg...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 130 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You feel your life force being drained.";
                Invoke("YouLose", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 131 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"You...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 131 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"You tricked me...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 131 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"I thought you were human...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 131 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "\"Heh heh...\"";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 4 && Selection == 131 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The man evaporates.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 150 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "He starts tearing chunks out of his greasy hair.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 156 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The man's shrieking only gets louder...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 156 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "More and more conspiracy theorists surround you.";
                Invoke("DisableButtons", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 159 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "You assert your dominance...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 159 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The fedora people and their hair grease all evaporate into thin air.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }

        if (Next == 0 && Selection == 160 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "The conspiracy theorists all start hissing at you...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 1 && Selection == 160 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "They each take a copy of Alex Jones' memoir...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 2 && Selection == 160 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "And start beating you with them...";
                Invoke("Continue", 0.1f);
                NextDialogue.Play();
            }
        if (Next == 3 && Selection == 160 && Input.GetKeyDown(KeyCode.Z))
            {
                Textbox.GetComponent<Text>().text = "Don't mess with conspiracy theorists.";
                Invoke("YouWin", buttondisabletime);
                NextDialogue.Play();
            }
        
        // This is for all the dialogue stuff
        if(disableButtons == true)
        {
            Option_1.SetActive(false);
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            Items.SetActive(false);
        }

        if(Selection == 1)
        {
            Option_1.GetComponentInChildren<Text>().text = "Start walking away";
            Option_2.GetComponentInChildren<Text>().text = "\"What's your problem?\"";
            Option_3.SetActive(false);
            ChoiceMade = 2;
        }

        if(Selection == 4)
        {
            Option_1.GetComponentInChildren<Text>().text = "Start running away";
            Option_2.GetComponentInChildren<Text>().text = "\"Who's Charlie Sheen?\"";
            Option_3.SetActive(false);
            ChoiceMade = 5;
        }

        if(Selection == 8)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"Why would he do that?\"";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 9;
        }

        if(Selection == 10)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"What's a lizard person?\"";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 125;
        }

        if(Selection == 50)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"I guess I am\"";
            Option_2.GetComponentInChildren<Text>().text = "\"Bah bah\"";
            Option_3.SetActive(false);
            ChoiceMade = 51;
        }

        if(Selection == 52)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"I am the lizard people\"";
            Option_2.GetComponentInChildren<Text>().text = "\"I've got to get to class\"";
            Option_3.GetComponentInChildren<Text>().text = "\"I don't care\"";
            ChoiceMade = 58;
        }

        if(Selection == 59)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"I'm going for Game Production\"";
            Option_2.GetComponentInChildren<Text>().text = "\"I'm going for Game Production\"";
            Option_3.GetComponentInChildren<Text>().text = "\"I'm going for Game Production\"";
            ChoiceMade = 70;
        }

        if(Selection == 61)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"Yes, I am\"";
            Option_2.GetComponentInChildren<Text>().text = "\"No, I'm not\"";
            Option_3.SetActive(false);
            ChoiceMade = 64;
        }

        if(Selection == 65)
        {
            Option_1.GetComponentInChildren<Text>().text = "Tell them you were kidding";
            Option_2.GetComponentInChildren<Text>().text = "Start hissing at them";
            Option_3.GetComponentInChildren<Text>().text = "Accept your fate";
            ChoiceMade = 68;
        }

        if(Selection == 71)
        {
            Option_1.GetComponentInChildren<Text>().text = "Tell him we're inside a video game";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 73;
        }

        if(Selection == 100)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"Yes, I am\"";
            Option_2.GetComponentInChildren<Text>().text = "\"No, I am not\"";
            Option_3.GetComponentInChildren<Text>().text = "\"I love big pharma\"";
            ChoiceMade = 101;
        }

        if(Selection == 105)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"Yeah sure\"";
            Option_2.GetComponentInChildren<Text>().text = "\"No thanks\"";
            Option_3.SetActive(false);
            ChoiceMade = 106;
        }

        if(Selection == 126)
        {
            Option_1.GetComponentInChildren<Text>().text = "\"I am a lizard person\"";
            Option_2.GetComponentInChildren<Text>().text = "Start eating a bug";
            Option_3.SetActive(false);
            ChoiceMade = 127;
        }

        if(Selection == 128)
        {
            Option_1.GetComponentInChildren<Text>().text = "Show him your rash";
            Option_2.GetComponentInChildren<Text>().text = "Swallow a big bug";
            Option_3.SetActive(false);
            ChoiceMade = 129;
        }

        if(Selection == 150)
        {
            Option_1.GetComponentInChildren<Text>().text = "Swallow the bug";
            Option_2.SetActive(false);
            Option_3.SetActive(false);
            ChoiceMade = 155;
        }

        if(Selection == 156)
        {
            Option_1.GetComponentInChildren<Text>().text = "Swallow another bug";
            Option_2.GetComponentInChildren<Text>().text = "Apologize";
            Option_3.SetActive(false);
            ChoiceMade = 158;
        }
    }

    void Continue()
    {
        Next += 1;
    }

    void YouWin()
    {
        if(!YouWon)
        {
            YouWinScreen.SetActive(true);
            Invoke("NextScene", 5.0f);
            BossMusic.Stop();
            YouWinSound.Play();
            YouWon = true;
        }
    }

    void YouLose()
    {
        if(!YouLost)
        {
            YouLoseScreen.SetActive(true);
            Invoke("ResetScene", 5.0f);
            BossMusic.Stop();
            YouLoseSound.Play();
            YouLost = true;
        }
    }

    void NextScene()
    {
        SceneManager.LoadScene("Classroom");
    }

    void ResetScene()
    {
        SceneManager.LoadScene("AntiVaxPete");
    }

    void DisableButtons()
    {
        Option_1.SetActive(true);
        Option_2.SetActive(true);
        Option_3.SetActive(true);
        Items.SetActive(true);
        disableButtons = false;
    }

    void TurnOnItems()
    {
        if(PlayerStart.JuulPickedUp == false)
        {
            Item_1.SetActive(false);
        }
        else
        {
            Item_1.SetActive(true);
        }

        if(PlayerStart.SlimPickedUp == false)
        {
            Item_2.SetActive(false);
        }
        else
        {
            Item_2.SetActive(true);
        }
        
        if(PlayerStart.AlexPickedUp == false)
        {
            Item_3.SetActive(false);
        }
        else
        {
            Item_3.SetActive(true);
        }
        BackButton.SetActive(true);
    }

    void TurnOffItems()
    {
        Item_1.SetActive(false);
        Item_2.SetActive(false);
        Item_3.SetActive(false);
        BackButton.SetActive(false);
    }

    void CannotUse()
    {
        Textbox.SetActive(false);
        UsageText.SetActive(true);
        Invoke("ReturnTextbox", 1.0f);
    }

    void ReturnTextbox()
    {
        Textbox.SetActive(true);
        UsageText.SetActive(false);
    }
}

