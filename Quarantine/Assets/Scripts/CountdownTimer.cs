﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 60f;
    public static bool startTimer;
    public GameObject YouLoseScreen;

    [SerializeField] Text countdownText;

    void Start()
    {
        currentTime = startingTime;
        startTimer = false;
        YouLoseScreen.SetActive(false);
    }

    void Update()
    {
        if(startTimer == true)
        {
            currentTime -= 1 * Time.deltaTime;
            countdownText.text = currentTime.ToString("0");

            if(currentTime <= 0)
            {
                currentTime = 0;
                YouLoseScreen.SetActive(true);
                Invoke("YouLose", 1.0f);
            }
        }
    }

    void YouLose()
    {
        YouLoseScreen.SetActive(true);
        Invoke("ResetScene", 5.0f);
    }

    void ResetScene()
    {
        if(PlayerStart.SceneNumber == 2)
        {
            SceneManager.LoadScene("JuulKidBattle");
        }

        if(PlayerStart.SceneNumber == 3)
        {
            SceneManager.LoadScene("AntiVaxPete");
        }
    }
}
