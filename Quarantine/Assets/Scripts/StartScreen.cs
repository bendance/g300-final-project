﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    public GameObject playButton;
    public GameObject quitButton;
    public AudioSource Selected;

    void Start()
    {
        playButton.SetActive(true);
        quitButton.SetActive(true);
    }

    public void PlayTheGame()
    {
        Invoke("LoadTheGame", 2.0f);
        Selected.Play();
    }

    public void QuitTheButton()
    {
        Application.Quit();
    }

    public void LoadTheGame()
    {
        SceneManager.LoadScene("Apartment");
    }
}
